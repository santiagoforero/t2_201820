package model.data_structures;

public class Nodo<T> 
{

	//-----------
	//Atributos
	//-----------
	
	/*
	 * Elemento del nodo
	 */
	private T elemento;
	
	/*
	 * Siguiente nodo encadenado
	 */
	private Nodo<T>siguiente;
	
	/*
	 * Anterior nodo encadenado
	 */
	private Nodo<T>anterior;
	
	//-----------
	//Constructor
	//-----------
	
	public Nodo(T pElemento)
	{
		elemento=pElemento;
		siguiente=null;
		anterior=null;
	}
	/**
     * Retorna el elemento que se encuentra en el nodo. <br>
     * <b>post: </b> Se retornó el elemento del nodo.<br>
     * @return Elemento del nodo<br>
     */
    public T darElemento( )
    {
        return elemento;
    }

    /**
     * Retorna el siguiente nodo en la lista. <br>
     * <b>post: </b> Se retornó el siguiente nodo en la lista.<br>
     * @return Siguiente nodo en la lista. Puede ser null<br>
     */
    public Nodo<T> darSiguiente( )
    {
        return siguiente;
    }

    /**
     * Retorna el nodo anterior en la lista. <br>
     * <b>post: </b>Se retornó el nodo anterior en la lista.<br>
     * @return Nodo anterior en la lista. Puede ser null<br>
     */
    public Nodo<T> darAnterior( )
    {
        return anterior;
    }

    /**
     * Inserta el nodo antes del actual. <br>
     * <b>pre: </b> nodo!=null. <br>
     * <b>post: </b> Se insertó el nodo especificado antes del actual.<br>
     * @param nuevo Nodo a insertar.<br>
     */
    public void insertarAntes( Nodo<T> nuevo )
    {
        nuevo.cambiarSiguiente(this); 
        nuevo.cambiarAnterior(anterior); 
        if( anterior != null )
        {
        	anterior.cambiarSiguiente(nuevo); 
        this.cambiarAnterior(nuevo); 
        }
    }

    /**
     * Inserta el nodo después del actual. <br>
     * <b>pre: </b> nodo!=null. <br>
     * <b>post: </b> Se insertó el nodo especificado después del nodo actual.<br>
     * @param nodo Nodo a insertar<br>
     */
    public void insertarDespues( Nodo<T> nodo )
    {
        nodo.siguiente = siguiente;
        nodo.anterior = this;
        if( siguiente != null )
            siguiente.anterior = nodo;
        siguiente = nodo;
    }
    /**
	 * Método que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(Nodo<T> siguiente)
	{
		this.siguiente = siguiente;
	}

	/**
	 * Método que cambia el nodo anterior por el que llega como parámetro.
	 * @param anterior Nuevo nodo anterior.
	 */
	public void cambiarAnterior(Nodo<T> pAnterior)
	{
		this.anterior = pAnterior;
	}
    /**
     * Desconecta el nodo de la lista suponiendo que es el primero. <br>
     * <b>pre: </b> El nodo es el primero de la lista. <br>
     * <b>post: </b> Se desconectó el nodo de la lista, sigNodo= null y antNodo= null.<br>
     * @return Nodo con el cual comienza la lista ahora<br>
     */
    public Nodo<T> desconectarPrimero( )
    {
        Nodo<T> p = siguiente;
        siguiente = null;
        if( p != null )
        {
            p.anterior = null;
        }
        return p;
    }

    /**
     * Desconecta el nodo de la lista suponiendo que no es el primero. <br>
     * <b>pre: </b> El nodo no es el primero de la lista. <br>
     * <b>post: </b> El nodo fue desconectado de la lista.<br>
     */
    public void desconectarNodo( )
    {
        Nodo<T> ant = anterior;
        Nodo<T> sig = siguiente;
        anterior = null;
        siguiente = null;
        ant.siguiente = sig;
        if( sig != null )
        {
            sig.anterior = ant;
        }
    }

    /**
     * Convierte el nodo a un String. <br>
     * <b>post: </b> Se retornó la representación en String del nodo. Dicha representación corresponde a la representación en String del elemento que contiene. <br>
     * @return La representación en String del nodo<br>
     */
    @Override
    public String toString( )
    {
        return elemento.toString( );
    }
}

