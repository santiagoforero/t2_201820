package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	int getSize();
	
	public boolean add(T elem);
	
	public boolean addAtEnd(T elem);
	
	public boolean addAtK(T elem,int pos);
	
	public T getElement(int pos);
	
	public T getCurrentElement();
	
	public boolean delete(T elemento);
	
	public boolean deleteAtK(int pos);
	
	public Nodo<T> next();
	
	public Nodo<T> previous();
	
	

}
