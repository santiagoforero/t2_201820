package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {


	//
	//Atributos
	//

	private Nodo<T> primero;

	private Nodo<T> ultimo;

	private Nodo<T> actual;

	private int cantidadElem;

	private Iterator<T>iter;

	public  DoublyLinkedList() 
	{
		cantidadElem=0;
		actual=null;
		primero=null;		
		ultimo=null;
	}
	public  DoublyLinkedList(T elemento) 
	{
		if(elemento==null)
		{
			throw new NullPointerException("Se recibío un elemento nulo");
		}
		cantidadElem=1;
		actual=new Nodo<T>(elemento);
		primero=new Nodo<T>(elemento);	
		ultimo=new Nodo<T>(elemento);	
	}

	@Override
	public int getSize() 
	{
		return cantidadElem;
	}

	@Override
	public Iterator<T> iterator() {
		return iter;


	}

	@Override
	public boolean add(T elemento) 
	{
		Nodo<T> nuevo = new Nodo<T>(elemento);
		boolean agregado=false;
		if(elemento==null)
		{
			throw new NullPointerException(); 
		}
		if(primero==null)
		{
			primero=nuevo;
			ultimo=nuevo;
			cantidadElem++;
			agregado=true;
		}
		else 
		{
			Nodo<T> var= primero;
			primero=nuevo;
			nuevo.cambiarSiguiente(var);
			var.cambiarAnterior(nuevo);
			agregado=true;
			cantidadElem++;
		}
		return agregado;

	}

	@Override
	public boolean addAtEnd(T elemento) {

		Nodo<T> nuevo = new Nodo<T>(elemento);
		boolean agregado=false;
		if(elemento==null)
		{
			throw new NullPointerException(); 
		}
		if(primero==null)
		{
			primero=nuevo;
			ultimo=nuevo;
			cantidadElem++;
			agregado=true;
		}
		else 
		{
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
			agregado=true;
			cantidadElem++;
		}
		return agregado;	}

	@Override
	public boolean addAtK(T elemento, int pos) {
		boolean respuesta = false;
		Nodo<T> nuevo = new Nodo<T>(elemento);
		Nodo <T> actual = primero;
		if (pos<=cantidadElem)
		{
			while(pos>0)
			{
				actual=actual.darSiguiente();
				pos --;
			}
			nuevo.cambiarSiguiente(actual);
			nuevo.cambiarAnterior(actual.darAnterior());
			if(actual.darAnterior() != null)
			{
				actual.darAnterior().cambiarSiguiente(nuevo);
				actual.cambiarAnterior(nuevo);
			}
			respuesta = true;
			cantidadElem ++;
		}
		else
		{
			throw new IndexOutOfBoundsException();
		}

		return respuesta;

	}

	@Override
	public T getElement(int pos) 
	{
		Nodo<T> actual=primero;
		if(pos<=cantidadElem)
		{
			while(pos>0)
			{
				actual=actual.darSiguiente();
				pos--;
			}

		}
		return (T) actual.darElemento();
	}

	@Override
	public T getCurrentElement() {

		return (T) actual.darElemento();
		
	}

	@Override
	public boolean delete(T elemento) 
	{
		boolean respuesta=false;
		Nodo<T> actual = primero;
		int contador=0;
		while(contador<cantidadElem)
		{
			contador++;
			if(actual.darElemento().equals(elemento))
			{
				if (actual==primero)
				{
					Nodo<T> var = actual;
					var.desconectarPrimero();
					respuesta=true;
					cantidadElem--;

				}
				else
				{
					Nodo<T> var = actual;
					var.desconectarNodo();
					respuesta=true;
					cantidadElem--;
				}
			}
			else
			{
				throw new ArrayIndexOutOfBoundsException("No se encontro el elemento");
			}
		}
		return respuesta;

	}

	@Override
	public boolean deleteAtK(int pos)
	{
		boolean resp=false;
		Nodo<T> actual=primero;
		if (pos<=cantidadElem)
		{
			while(pos>0)
			{
				actual=actual.darSiguiente();
				pos --;
			}
			if (actual==primero)
			{
				Nodo<T> var = actual.darSiguiente();
				var.desconectarPrimero();
				cantidadElem--;
				resp=true;
			}
			else
			{
				actual.desconectarNodo();
				cantidadElem--;
				resp=true;
			}
		}
		else
		{
			throw new IndexOutOfBoundsException();
		}
		return resp;		
	}

	@Override
	public Nodo<T> next() {
		// TODO Auto-generated method stub
		return actual.darSiguiente();
	}

	@Override
	public Nodo<T> previous() {
		// TODO Auto-generated method stub
		return actual.darAnterior();
	}

}
