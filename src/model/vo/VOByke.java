package model.vo;

/**
 * Representation of a byke object
 */
public class VOByke {

	private int id;
	private String nombre;
	private String ciudad;
	private int latitud;
	private int longitud;
	private int dpCapacidad;
	private String fecha;
	
	public VOByke(int pId,String pNombre,String pCiudad,int pLatitud,int pLongitud,int pCapacidad,String pFecha)
	{
		id=pId;
		nombre=pNombre;
		ciudad=pCiudad;
		latitud=pLatitud;
		longitud=pLongitud;
		dpCapacidad=pCapacidad;
		fecha=pFecha;
	}
	/**
	 * @return id_bike - Bike_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return 0;
	}	
	public String darNombre()
	{
		return nombre;
	}
	public int darLatitud()
	{
		return latitud;
	}
	public int darLongitud()
	{
		return longitud;
	}
	public String darCiudad()
	{
		return ciudad;
	}
	public int darCapacidad()
	{
		return dpCapacidad;
	}
	public String darFecha()
	{
		return fecha;
	}
}
